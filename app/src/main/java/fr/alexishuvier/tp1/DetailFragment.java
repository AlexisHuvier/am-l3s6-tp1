package fr.alexishuvier.tp1;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import fr.alexishuvier.tp1.data.Country;

public class DetailFragment extends Fragment {

    TextView nameView;
    ImageView imageView;
    EditText capitalEdit;
    EditText languageEdit;
    EditText monnaieEdit;
    EditText populationEdit;
    EditText superficieEdit;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_second, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        nameView = view.findViewById(R.id.nameView2);
        imageView = view.findViewById(R.id.imageView2);
        capitalEdit = view.findViewById(R.id.capitalEdit);
        languageEdit = view.findViewById(R.id.languageEdit);
        monnaieEdit = view.findViewById(R.id.monnaieEdit);
        populationEdit = view.findViewById(R.id.populationEdit);
        superficieEdit = view.findViewById(R.id.superficieEdit);

        int position = DetailFragmentArgs.fromBundle(getArguments()).getCountryId();
        Country country = Country.countries[position];

        Context c = imageView.getContext();
        imageView.setImageDrawable(c.getResources().getDrawable(c.getResources(). getIdentifier (country.getImgUri() , null , c.getPackageName())));
        nameView.setText(country.getName());
        capitalEdit.setText(country.getCapital());
        languageEdit.setText(country.getLanguage());
        monnaieEdit.setText(country.getCurrency());
        populationEdit.setText(String.valueOf(country.getPopulation()));
        superficieEdit.setText(country.getArea()+ " km²");


        view.findViewById(R.id.button_second).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(DetailFragment.this)
                        .navigate(R.id.action_SecondFragment_to_FirstFragment);
            }
        });
    }
}